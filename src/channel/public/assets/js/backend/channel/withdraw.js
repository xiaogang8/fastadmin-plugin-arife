define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'channel/withdraw/index' + location.search,
                    add_url: 'channel/withdraw/add',
                    edit_url: 'channel/withdraw/edit',
                    del_url: 'channel/withdraw/del',
                    multi_url: 'channel/withdraw/multi',
                    import_url: 'channel/withdraw/import',
                    table: 'channel_withdraw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'channel.truename', title: __('渠道名称'), operate: 'LIKE'},
                        {field: 'total_fee', title: __('提现金额(元)'), operate:'BETWEEN'},
                        {field: 'withdraw_type', title: __('Withdraw_type'), operate: 'LIKE'},
                        {field: 'withdraw_account', title: __('Withdraw_account'), operate: 'LIKE'},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1'),"2":__('Status 2')}, formatter: Table.api.formatter.status},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: 'truename', title: __('收款人姓名'), operate: 'LIKE'},
                        {field: 'admin.nickname', title: __('处理人'), operate: 'LIKE'},
                        // {field: 'buttons',title:__('操作'),table:table,events:Table.api.events.operate,buttons:[
                        //     {
                        //         name:'check',
                        //         text:''
                        //     }
                        // ],formatter: Table.api.formatter.buttons}
                        // {field: 'admin.nickname', title: __('Admin.nickname'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            $("#btn-withdraw").click(function(){
                Fast.api.open('channel/withdraw/withdraw','提现申请',{})
            })
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        withdraw: function(){
            $("#c-bank_id").on('change',function(obj){
                // $.post("channel/withdraw/bankinfo",{
                //     bank_id:$("#c-bank_id").val(),
                // },function(res){
                //     console.log(res);
                //     if(res.code == 1){
                //         $("#c-withdraw_account").val(res.data.account);
                //         // $("#c-withdraw_account").val(res.data.account);
                //     }else{
                //         layer.msg(res.msg);
                //     }
                // });
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});