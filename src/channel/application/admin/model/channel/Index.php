<?php

namespace app\admin\model\channel;

use think\Model;
use fast\Xxtea;

class Index extends Model
{
    // 表名
    protected $name = 'channel';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    private $key = 'ydguard2021';

    // 追加属性
    protected $append = [
        'status_text'
    ];
    

    
    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1'), '2' => __('Status 2')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function encrypt($s){
        return Xxtea::encrypt($s,$this->key);
    }

    public function decrypt($s){
        return Xxtea::decrypt($s,$this->key);
    }

    /**
     * 余额变动
     *
     * @param int $channel_id
     * @param float $total_fee
     * @return void
     * @author AriFe.Liu 
     */
    public static function money($channel_id,$total_fee,$memo,$admin_id){
        $channel = self::where(['id'=>$channel_id])->find();
        $before = $channel['money'];
        $after = $before + $total_fee;
        $channel->money = $after;
        $channel->save();
        Moneylog::ins($channel_id,$total_fee,$before,$memo,$admin_id);
    }

}
