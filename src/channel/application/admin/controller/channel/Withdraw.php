<?php

namespace app\admin\controller\channel;

use app\common\controller\Backend;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 提现记录
 *
 * @icon fa fa-circle-o
 */
class Withdraw extends Backend
{
    
    /**
     * Withdraw模型对象
     * @var \app\admin\model\channel\Withdraw
     */
    protected $model = null;
    protected $noNeedRight = ['banklist'];
    protected $dataLimit = true;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\channel\Withdraw;
        $this->view->assign("statusList", $this->model->getStatusList());
        $this->view->assign("overview",$this->model->getOverview($this->auth->id));
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                    ->with(['channel','admin'])
                    ->where($where)
                    ->order($sort, $order)
                    ->paginate($limit);

            foreach ($list as $row) {
                $row->visible(['id','createtime','total_fee','withdraw_type','withdraw_account','status','updatetime','remark','truename','mobile']);
                $row->visible(['channel']);
				// $row->getRelation('channel')->visible(['truename']);
				$row->visible(['admin']);
				$row->getRelation('admin')->visible(['nickname']);
            }

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    #################

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }                    
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }        
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }       
                    if($row->status != '0'){
                        exception('该记录已处理!');
                    }
                    if($params['status']!='0'){
                        $params['op_admin_id'] = $this->auth->id;
                    }             
                    if($params['status'] == '2'){
                        if(empty($params['remark'])){
                            exception("请填写驳回理由");
                        }
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 提现
     *
     * @return void
     * @author AriFe.Liu 
     */
    public function withdraw(){
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $bank_id = isset($params['bank_id']) ? $params['bank_id']:null;
            if(!$bank_id) $this->error('请选择提现方式!');
            $total_fee = isset($params['total_fee']) ? $params['total_fee']:null;
            if(!$total_fee) $this->error('请输入提现金额!');
            $channel_id = $params['channel_id'];

            try {
                $this->model->withdraw($channel_id,$total_fee,$bank_id,$this->auth->id);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }

            $this->success('申请成功,请耐心等待管理员审核发放,谢谢!');
        }else{
            $channel = db('Channel')->where(['admin_id'=>$this->auth->id])->find();
            $channel_id = $channel['id'];
            $balance = $channel['money'];
            $withdraw_min = config('site.withdraw_min');
            $withdraw_max = config('site.withdraw_max');
            $this->view->assign('balance',$balance);
            $this->view->assign('channel_id',$channel_id);
            $this->view->assign('withdraw_min',$withdraw_min);
            $this->view->assign('withdraw_max',$withdraw_max);
            return $this->view->fetch();
        }
        
    }

    public function banklist(){
        $channel_id = db('Channel')->where(['admin_id'=>$this->auth->id])->value('id');
        $list = db('ChannelBank')->where(['channel_id'=>$channel_id])->field("id,bank_name,account")->select();
        $total = count($list);
        echo json_encode([
            'list' => $list,
            'total' => $total,
        ]);
    }

    public function bankinfo($bank_id=null){
        $info = db('ChannelBank')->where(['id'=>$bank_id])->field("id,account,truename,mobile,bank_name")->find();
        return $info;
    }

}
