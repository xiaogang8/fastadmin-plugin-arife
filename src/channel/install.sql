CREATE TABLE `__PREFIX__channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级渠道ID',
  `level` char(1) NOT NULL DEFAULT '1' COMMENT '渠道等级',
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '关联账户',
  `money` decimal(10,0) NOT NULL DEFAULT '0' COMMENT '余额',
  `mobile` varchar(18) NOT NULL COMMENT '手机号码',
  `idcard` varchar(18) NOT NULL COMMENT '身份证号码',
  `ratio` varchar(255) DEFAULT NULL COMMENT '分佣比例',
  `truename` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '状态:0=待审核,1=已审核,2=已驳回',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `code` varchar(32) DEFAULT NULL COMMENT '识别码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='渠道列表';

CREATE TABLE `__PREFIX__channel_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `channel_id` int(11) NOT NULL COMMENT '关联渠道',
  `type` enum('bank','alipay') NOT NULL DEFAULT 'bank' COMMENT '收款方式:bank=银行卡,alipay=支付宝',
  `account` varchar(255) NOT NULL COMMENT '账号',
  `truename` varchar(255) NOT NULL COMMENT '姓名',
  `mobile` varchar(18) NOT NULL COMMENT '手机号',
  `bank_name` varchar(255) DEFAULT NULL COMMENT '开户行名称',
  `admin_id` int(10) DEFAULT NULL COMMENT '账户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='提现方式';

CREATE TABLE `__PREFIX__channel_commission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `channel_id` int(11) NOT NULL COMMENT '渠道ID',
  `total_fee` decimal(10,0) NOT NULL COMMENT '佣金',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `rel_table` varchar(255) DEFAULT NULL COMMENT '关联表',
  `rel_id` int(11) DEFAULT NULL COMMENT '关联表主键',
  `admin_id` int(11) DEFAULT NULL COMMENT '关联账户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='佣金明细';

CREATE TABLE `__PREFIX__channel_money_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更余额',
  `before` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更前余额',
  `after` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更后余额',
  `memo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `createtime` int(10) NOT NULL COMMENT '创建时间',
  `admin_id` int(11) NOT NULL COMMENT '关联账户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='余额明细';

CREATE TABLE `__PREFIX__channel_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `poster_image` varchar(500) NOT NULL COMMENT '海报底图',
  `url` varchar(255) DEFAULT NULL COMMENT '推广链接',
  `xpos` int(11) DEFAULT '0' COMMENT 'X坐标',
  `ypos` int(11) DEFAULT '0' COMMENT 'Y坐标',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '启用状态:0=禁用,1=启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='推广海报';

CREATE TABLE `__PREFIX__channel_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `channel_id` int(11) NOT NULL COMMENT '渠道ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户列表';

CREATE TABLE `__PREFIX__channel_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `channel_id` int(11) NOT NULL COMMENT '所属渠道',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `total_fee` decimal(10,0) NOT NULL DEFAULT '0' COMMENT '金额',
  `withdraw_type` varchar(255) DEFAULT NULL COMMENT '提现方式',
  `withdraw_account` varchar(255) DEFAULT NULL COMMENT '提现账号',
  `truename` varchar(255) NOT NULL COMMENT '真实姓名',
  `mobile` varchar(18) DEFAULT NULL COMMENT '手机号',
  `status` enum('0','1','2') NOT NULL COMMENT '状态:0=待审核,1=已审核,2=已驳回',
  `op_admin_id` varchar(255) DEFAULT NULL COMMENT '操作员',
  `updatetime` int(11) DEFAULT NULL COMMENT '处理时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `admin_id` int(11) NOT NULL COMMENT '关联账户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='提现记录';

INSERT INTO `__PREFIX__config`(`name`, `group`, `title`, `tip`, `type`, `value`, `content`, `rule`, `extend`, `setting`) VALUES ('withdraw_min', 'withdraw', '最低提现金额(元)', '大于等于此金额配置才能发起提现申请', 'number', '100', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');
INSERT INTO `__PREFIX__config`(`name`, `group`, `title`, `tip`, `type`, `value`, `content`, `rule`, `extend`, `setting`) VALUES ('withdraw_max', 'withdraw', '最大提现金额(元)', '小于等于此金额配置,才能发起提现申请', 'number', '10000', '', '', '', '{\"table\":\"\",\"conditions\":\"\",\"key\":\"\",\"value\":\"\"}');

ALTER TABLE `sh_channel_user` 
ADD COLUMN `admin_id` int(0) NOT NULL AFTER `createtime`;