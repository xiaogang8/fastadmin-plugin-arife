<?php

namespace app\admin\model\channel;

use think\Model;


class Commission extends Model
{

    

    

    // 表名
    protected $name = 'channel_commission';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function channel()
    {
        return $this->belongsTo('app\admin\model\channel\Index', 'channel_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    /**
     * 多态关联
     * 
     * 根据插件配置映射的关联模型来取出对应数据
     *
     * 注意: 如果插件配置中没有对当前表已经使用的模型做关联,或关联错误找不到指定模型时, tp5会抛出异常造成程序中止
     * 我开发此插件时已将tp源代码进行修改, 修改之后出现错误时程序不会报错, 但是同样的, 方法返回的数据中此条记录也没有了relinfo字段
     * 具体修改内容为: 
     * ---------------
     * 文件名: thinkphp/libiary/think/model/relation/MorphTo.php
     * 176行: if(!class_exists($model)){
     *            continue;
     *        } 
     *        $obj   = new $model;
     * 190行: // throw new Exception('relation data not exists :' . $this->model);
     *        continue;
     * ---------------
     * 另外, 如果使用一对一关联, morphOne方法的话, 有可能会出现同样问题, 修改方法参见下方文档评论处
     * https://www.kancloud.cn/manual/thinkphp5/250866
     *
     * @return void
     * @author AriFe.Liu 
     */
    public function relinfo(){ 
        $conf = get_addon_config('channel');
        $alias = $conf['relmodule'];        
        return $this->morphTo(['rel_table','rel_id'],$alias);
    }

    /**
     * 插入佣金记录
     *
     * @return void
     * @author AriFe.Liu 
     */
    public function ins($channel_id,$admin_id,$total_fee,$rel_table,$rel_id,$memo='佣金'){
        # 插入必要信息
        $res = $this->insert([
            'channel_id' => $channel_id,
            'total_fee' => $total_fee,
            'createtime' => time(),
            'rel_table' => $rel_table,
            'rel_id' => $rel_id
        ]);
        if(!$res) exception('插入佣金记录失败');
        # 余额变动
        Index::money($channel_id,$total_fee,$memo,$admin_id);
    }
}
